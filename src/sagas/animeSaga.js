import { call, put, fork, take } from 'redux-saga/effects';
import {
    FETCH_ANIME,
    fetchAnimeStart,
    fetchAnimeFinish,
    fetchAnimeSuccess,
} from '../redux/animelist';
import { setErrorMessage } from '../redux/error';
import { getAnime} from '../services/api';

function* animeSaga() {
    while (true) {
        const action = yield take(FETCH_ANIME);
        try {
            yield put(fetchAnimeStart());
            const animeData = yield call(
                getAnime
            );

            if (__DEV__) console.log("Data Anime is ", animeData);

            yield put(fetchAnimeSuccess(animeData))
        } catch (err) {
            console.log('ERROR GET ANIME DATA ', err);
            fetchAnimeFinish(err)
        }
    }
}


export default function* wacher() {
    yield fork(animeSaga);
}

