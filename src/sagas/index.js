import { takeEvery, fork, all } from 'redux-saga/effects';
import authSaga from './authSaga';
import animeSaga from './animeSaga';
import movieSaga from './movieSaga';

export default function* rootSaga() {
  yield all([
      fork(authSaga),
      fork(animeSaga),
      fork(movieSaga),
  ]);
}
