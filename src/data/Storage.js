import AsyncStorage from '@react-native-community/async-storage';

const USER_DATA = 'USER_DATA';
const TOKEN_DATA_KEY = 'TOKEN_DATA';


export default class Storage {
    static async setUser(userData) {
        try {
            await AsyncStorage.setItem(USER_DATA, JSON.stringify(userData));
        } catch (error) {
            throw new Error('Error While Saving User Data');
        }
    }

    static async getUser() {
        try {
            const userData = await AsyncStorage.getItem(USER_DATA);
            return JSON.parse(userData);
        } catch (error) {
            throw new Error('Error While Getting User Data');
        }
    }

    static async resetUser() {
        try {
            await AsyncStorage.removeItem(USER_DATA);
        } catch {
            throw new Error('Error while Reseting User Type');
        }
    }

    static async setToken(token) {
        try {
          await AsyncStorage.setItem(TOKEN_DATA_KEY, token);
        } catch (error) {
          throw new Error('Error While Saving Token');
        }
      }
    
      static async resetToken() {
        try {
          await AsyncStorage.removeItem(TOKEN_DATA_KEY);
        } catch (error) {
          throw new Error('error while reseting token');
        }
      }
    
      static async getToken() {
        try {
          const accessToken = await AsyncStorage.getItem(TOKEN_DATA_KEY);
          return accessToken;
        } catch (error) {
          throw new Error('Error While getting Token');
        }
      }
    
}