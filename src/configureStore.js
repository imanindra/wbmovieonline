import {createStore, applyMiddleware, compose} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import immutableTransform from 'redux-persist-transform-immutable';
import devToolsEnhancer from 'remote-redux-devtools';
import createSagaMiddleware from 'redux-saga';
import {createLogger} from 'redux-logger';
import AsyncStorage from '@react-native-community/async-storage';
import reducers from './redux';
import sagas from './sagas';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];
if (__DEV__ === true) {
  middlewares.push(createLogger({collapsed: true}));
}

const persistConfig = {
  transforms: [immutableTransform()],
  key: 'root',
  storage: AsyncStorage,
  whitelist : [],
  blacklist : []
};

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = compose(applyMiddleware(...middlewares))(createStore)(
  persistedReducer,
);
export const persistor = persistStore(store);

sagaMiddleware.run(sagas);
