export default {
    COLOR: {
        PRIMARY :'#005D9E',
        SECONDARY: "#e9b128",
        WHITE: "white",
        BLACK : 'black',
        TEXTINPUT : '#F4F4F4'
    },
}