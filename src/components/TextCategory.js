import React from 'react'
import { View, Text, Image } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux'
import assets from '../assets';



class TextCategory extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { title } = this.props
        return (
            <View style={{ flexDirection: 'row', marginBottom: hp(3),marginTop : hp(3) }}>
                <Image
                    style={{ width: wp(5), height: wp(5), marginHorizontal: wp(3) }}
                    source={assets.icon_playlist}
                />
                <Text>{title}</Text>
            </View>
        )
    }
}

export default TextCategory

