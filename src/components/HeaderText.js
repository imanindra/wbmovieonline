import React from 'react'
import { View, Text, StyleSheet, Image, Dimensions } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import assets from '../assets'
import Constant from '../constant/constant'
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        height: height * 0.1,
        backgroundColor: Constant.COLOR.PRIMARY,
        alignContent:'center',
        justifyContent: 'center',
        shadowColor: "#000000",
        shadowOpacity: 0.8,
        shadowRadius: 2,
        shadowOffset: {
            height: 1,
            width: 1
        }
    },
})

class HeaderText extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { Title } = this.props
        return (
            <View style={styles.container} elevation={1}>
                    <Text style={{
                        fontSize : 20,
                        marginLeft : 10,
                        color : 'white'
                    }}>{Title}</Text>
            </View>
        )
    }
}

export default HeaderText
