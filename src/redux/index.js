import { combineReducers } from 'redux';
import authReducer from './auth';
import errorReducer from './error';
import movieReducer from './movielist';
import animeReducer from './animelist';

const reducers = combineReducers({
    auth: authReducer,
    error: errorReducer,
    movie: movieReducer,
    anime: animeReducer
});

export default reducers;
