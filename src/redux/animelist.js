import { Map } from 'immutable';

export const FETCH_ANIME = 'animelist/fetchMovie'
export const FETCH_ANIME_START = 'animelist/fetchAnimeStart'
export const FETCH_ANIME_SUCCESS = 'animelist/fetchAnimeSuccess'
export const FETCH_ANIME_FILTER = 'animelist/fetchAnimeFilter'
export const FETCH_ANIME_FINISH = 'animelist/fetchAnimeFinish'

export function fetchAnime() {
    return {
        type: FETCH_ANIME,
    }
}

export function fetchAnimeStart() {
    return {
        type: FETCH_ANIME_START,
    }
}

export function fetchAnimeSuccess(payload) {
    return {
        type: FETCH_ANIME_SUCCESS,
        payload
    }
}

export function fetchAnimeFinish() {
    return {
        type: FETCH_ANIME_FINISH
    }
}

const initialState = Map({
    isFetching: false,
    isSuccess: false,
    data: [],
})

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_ANIME_START:
            return state.set('isFetching', true);
        case FETCH_ANIME_SUCCESS:
            return state.withMutations(currentState =>
                currentState
                    .set('isFetching', false)
                    .set('isSuccess', true)
                    .set('data', action.payload)
            );
        case FETCH_ANIME_FINISH:
            return state.withMutations(currentState =>
                currentState.set('isFetching', false).set('isSuccess', false),
            );
        default:
            return state;
    }
}

export const getIsFetchingAnime = state => state.get('isFetching');
export const getDataAnime = state => state.get('data');
export const getIsSuccessAnime = state => state.get('isSuccess');



