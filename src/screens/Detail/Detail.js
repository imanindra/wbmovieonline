import React, { Component, useState } from 'react';
import {
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    View,
    Text,
    TextInput,
    Image,
    FlatList,
    ScrollView
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Rating } from 'react-native-ratings';
import { Actions } from 'react-native-router-flux';
import assets from '../../assets';

class Detail extends Component {
    constructor() {
        super();
        this.state = {
        }
    }

    componentDidMount() {
        this.getDetail()
        this.getSeeAlso()
    }


    getDetail() {
        const { dataDetail } = this.props
        fetch('https://api.gdriveplayer.us/v1/imdb/' + dataDetail.imdb)
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData != null) {
                    this.setState({
                        detailMovie: responseData
                    })
                } else {
                    alert('error')
                }
            })
    }

    getSeeAlso() {
        fetch('https://api.gdriveplayer.us/v1/movie/newest?page=')
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData != null) {
                    this.setState({
                        dataSeeAlso: responseData
                    })
                } else {
                    alert('error')
                }
            })
    }

    render() {
        const { detailMovie, dataSeeAlso } = this.state
        return (
            <ScrollView>
                <TouchableOpacity
                    onPress={() => Actions.pop()}
                    style={{ position: 'absolute', zIndex: 99, margin: wp(5) }}>
                    <Image
                        style={{ width: wp(7), height: wp(7) }}
                        source={assets.icon_arrow}
                    />
                </TouchableOpacity>
                {detailMovie != null &&
                    <View style={{ alignItems: 'center' }}>
                        <View>
                            <Image
                                style={{ width: wp(100), height: hp(50) }}
                                source={{ uri: `${detailMovie.Poster}` }}
                            />

                            <View style={{ position: 'absolute', alignSelf: 'center', marginTop: hp(39), flexDirection: 'row' }}>
                                <TouchableOpacity
                                    style={styles.btnCircelSide}
                                    onPress={() => alert('share')}
                                >
                                    <Image
                                        style={{ width: wp(8), height: wp(8), alignSelf: 'center' }}
                                        source={assets.icon_share}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.btnCircelCenter}
                                    onPress={() => Actions.Play({ Play: `http://database.gdriveplayer.us/player.php?imdb=${detailMovie.imdbID}` })}
                                >
                                    <Image
                                        style={{ width: wp(16), height: wp(16), alignSelf: 'center' }}
                                        source={assets.icon_play}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.btnCircelSide}
                                    onPress={() => alert('zoom')}
                                >
                                </TouchableOpacity>
                            </View>
                        </View>
                        <Text style={styles.textTitleBig}>{detailMovie.Title}</Text>
                        <Text style={styles.textTitleGenre}>{detailMovie.Genre}</Text>

                        <Rating
                            readonly={true}
                            ratingCount={5}
                            imageSize={wp(5)}
                            startingValue={detailMovie.imdbRating / 2}
                        />
                        <Text>{detailMovie.imdbRating}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ marginHorizontal: wp(2), alignItems: 'center' }}>
                                <Text style={styles.textDetailMovie}>Year</Text>
                                <Text style={styles.textDetailMovie}>{detailMovie.Year}</Text>
                            </View>
                            <View style={{ marginHorizontal: wp(2), alignItems: 'center' }}>
                                <Text style={styles.textDetailMovie}>Duration</Text>
                                <Text style={styles.textDetailMovie}>{detailMovie.Runtime == '' ? '-- --' : detailMovie.Runtime}</Text>
                            </View>
                            <View style={{ marginHorizontal: wp(2), alignItems: 'center' }}>
                                <Text style={styles.textDetailMovie}>Country</Text>
                                <Text style={styles.textDetailMovie}>{detailMovie.Country}</Text>
                            </View>
                        </View>
                        <Text style={styles.textDescription}>
                            {detailMovie.Plot}
                        </Text>
                    </View>
                }

                <View style={{ flexDirection: 'row', marginBottom: hp(3) }}>
                    <Image
                        style={{ width: wp(5), height: wp(5), marginHorizontal: wp(3) }}
                        source={assets.icon_playlist}
                    />
                    <Text>SEE ALSO</Text>
                </View>


                <FlatList
                    data={dataSeeAlso}
                    horizontal={true}
                    renderItem={({ item }) =>
                        <TouchableOpacity
                            style={{ alignItems: 'center' }}
                            onPress={() => Actions.Detail({ dataDetail: item })}>
                            <Image
                                style={{ height: hp(35), width: wp(50), resizeMode: 'contain', alignSelf: 'center', }}
                                source={{ uri: `${item.poster}` }}
                            />
                            <Text style={{ width: wp(48), textAlign: 'center' }}>{item.title}</Text>
                            <Rating
                                readonly={true}
                                ratingCount={5}
                                imageSize={wp(5)}
                                startingValue={item.rating / 2}
                            />
                            <Text>{item.rating}</Text>
                        </TouchableOpacity>
                    }
                    keyExtractor={item => item.id}
                />

            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    btnCircelSide: {
        backgroundColor: 'black',
        opacity: 0.5,
        width: wp(15),
        height: wp(15),
        borderRadius: wp(15) / 2,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    btnCircelCenter: {
        backgroundColor: 'black',
        opacity: 0.5,
        width: wp(20),
        height: wp(20),
        borderRadius: wp(10),
        marginHorizontal: wp(7),
        justifyContent: 'center'
    },
    textTitleBig: {
        fontSize: wp(7),
        fontWeight: 'bold',
        width: wp(80),
        textAlign: 'center'
    },
    textTitleGenre: {
        fontSize: wp(4),
        marginVertical: hp(1)
    },
    textDetailMovie: {
        fontSize: wp(4.5),
        fontWeight: '900'
    },
    textDescription: {
        fontSize: wp(6),
        fontWeight: 'bold',
        textAlign: 'center',
        padding: wp(5)
    }
})



export default Detail 
