import React, { Component, useState } from 'react';
import {
    StyleSheet,
    SafeAreaView,
    TouchableOpacity,
    View,
    Text,
    TextInput,
    Image,
    FlatList,
    ScrollView
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Rating } from 'react-native-ratings';
import { Actions } from 'react-native-router-flux';
import assets from '../../assets';

class DetailAnime extends Component {
    constructor() {
        super();
        this.state = {
            detailMovie: [],
            tesMovie: [],
            dataEpisode: 1
        }
    }

    componentDidMount() {
        this.getDetailAnime()
        this.getSeeAlso()
    }


    getDetailAnime() {
        const { dataDetail } = this.props
        var tesMovie = [];
        fetch('https://api.gdriveplayer.us/v1/anime/id/' + dataDetail.id)
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData != null) {
                    this.setState({
                        detailMovie: responseData[0],
                        detailData: responseData,
                        totalEpisode: responseData[0].total_episode
                    })
                    for (let i = 0; i < this.state.totalEpisode; i++) {
                        tesMovie.push({
                            dataImage: responseData[0].poster,
                            nameAnime: responseData[0].title,
                            episode: i + 1
                        })
                        this.setState({
                            dataTes: tesMovie
                        })
                    }

                } else {
                    alert('error')
                }
            })
    }

    getSeeAlso() {
        fetch('https://api.gdriveplayer.us/v1/movie/newest?page=')
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData != null) {
                    this.setState({
                        dataSeeAlso: responseData
                    })
                } else {
                    alert('error')
                }
            })
    }

    render() {
        const { detailMovie } = this.state
        return (
            <ScrollView>
                <TouchableOpacity
                    onPress={() => Actions.pop()}
                    style={{ position: 'absolute', zIndex: 99, margin: wp(5) }}>
                    <Image
                        style={{ width: wp(7), height: wp(7) }}
                        source={assets.icon_arrow}
                    />
                </TouchableOpacity>
                {detailMovie != null &&
                    <View style={{ alignItems: 'center' }}>
                        <View>
                            <Image
                                style={{ width: wp(100), height: hp(70) }}
                                source={{ uri: `${detailMovie.poster}` }}
                            />
                            <View style={{ position: 'absolute', alignSelf: 'center', marginTop: hp(59), flexDirection: 'row' }}>
                                <TouchableOpacity
                                    style={styles.btnCircelSide}
                                    onPress={() => alert('share')}
                                >
                                    <Image
                                        style={{ width: wp(8), height: wp(8), alignSelf: 'center' }}
                                        source={assets.icon_share}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.btnCircelCenter}
                                    onPress={() => Actions.Play({ Play: `https://database.gdriveplayer.us/player.php?type=anime_indo&id=${detailMovie.id}&episode=` + this.props.dataEpisodeCount })}
                                >
                                    <Image
                                        style={{ width: wp(16), height: wp(16), alignSelf: 'center' }}
                                        source={assets.icon_play}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    style={styles.btnCircelSide}
                                    onPress={() => alert('zoom')}
                                >
                                </TouchableOpacity>
                            </View>
                        </View>
                        <Text style={styles.textTitleBig}>{detailMovie.title}</Text>
                        <Text style={styles.textTitleGenre}>{detailMovie.genre}</Text>
                        <Text style={styles.textDetailMovie}>{detailMovie.imdbRating}</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ marginHorizontal: wp(2), alignItems: 'center' }}>
                                <Text style={styles.textDetailMovie}>Status</Text>
                                <Text style={styles.textDetailMovie}>{detailMovie.status}</Text>
                            </View>
                            <View style={{ marginHorizontal: wp(2), alignItems: 'center' }}>
                                <Text style={styles.textDetailMovie}>Episode</Text>
                                <Text style={styles.textDetailMovie}>{this.props.dataEpisodeCount != 1 ? this.props.dataEpisodeCount : this.state.dataEpisode}</Text>
                            </View>
                            <View style={{ marginHorizontal: wp(2), alignItems: 'center' }}>
                                <Text style={styles.textDetailMovie}>Subtitle</Text>
                                <Text style={styles.textDetailMovie}>{detailMovie.sub}</Text>
                            </View>
                        </View>
                        <Text style={styles.textDescription}>
                            {detailMovie.summary}
                        </Text>
                    </View>
                }

                <View style={{ flexDirection: 'row', marginBottom: hp(3) }}>
                    <Image
                        style={{ width: wp(5), height: wp(5), marginHorizontal: wp(3) }}
                        source={assets.icon_playlist}
                    />
                    <Text>ALL EPISODES</Text>
                </View>

                <FlatList
                    data={this.state.dataTes}
                    horizontal={true}
                    renderItem={({ item }) =>
                        <TouchableOpacity
                            style={{ alignItems: 'center' }}
                            onPress={() => Actions.DetailAnime({ dataDetail: detailMovie, dataEpisodeCount: item.episode })}
                        >
                            <Image
                                style={{ height: hp(35), width: wp(50), resizeMode: 'contain', alignSelf: 'center', }}
                                source={{ uri: `${item.dataImage}` }}
                            />
                            <Text style={{ width: wp(48), textAlign: 'center' }}>{item.nameAnime}</Text>
                            <Text>Episode : {item.episode}</Text>
                        </TouchableOpacity>
                    }
                    keyExtractor={item => item.id}
                />

            </ScrollView>
        )
    }
}

export default DetailAnime

const styles = StyleSheet.create({
    btnCircelSide: {
        backgroundColor: 'black',
        opacity : 0.5,
        width: wp(15),
        height: wp(15),
        borderRadius: wp(15) / 2,
        alignSelf: 'center',
        justifyContent: 'center'
    },
    btnCircelCenter: {
        backgroundColor: 'black',
        opacity : 0.5,
        width: wp(20),
        height: wp(20),
        borderRadius: wp(10),
        marginHorizontal: wp(7),
        justifyContent: 'center'
    },
    textTitleBig: {
        fontSize: wp(7),
        fontWeight: 'bold',
        width: wp(80),
        textAlign: 'center'
    },
    textTitleGenre: {
        fontSize: wp(4),
        marginVertical: hp(1)
    },
    textDetailMovie: {
        fontSize: wp(4.5),
        fontWeight: '900'
    },
    textDescription: {
        fontSize: wp(6),
        fontWeight: 'bold',
        textAlign: 'center',
        padding: wp(5)
    }
})
