import React, { Component } from 'react';
import {
  View,
  Image,
  StatusBar,
} from 'react-native';
import Assets from '../../assets';
import Storage from '../../data/Storage'
import { Actions } from 'react-native-router-flux'
import remoteConfig from '@react-native-firebase/remote-config';


export default class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  async componentDidMount() {
    StatusBar.setHidden(true);
    await Storage.getToken().then(token => {
      if (token) {
        this.setState({
          token: token.accessToken,
          isInitiated: true,
        });
        setTimeout(function () {
          Actions.TabBar();
        }, 5000)
      } else {
        setTimeout(function () {
          Actions.Login();
        }, 5000)
      }
    });

    remoteConfig()
      .setDefaults({
        series: 'disabled',
      })
      .then(() => remoteConfig().fetchAndActivate())
      .then(activated => {
        if (activated) {
          console.log('Defaults set, fetched & activated!');
        } else {
          console.log('Defaults set, however activation failed.');
        }
      });

  }

  componentWillUnmount() {
    StatusBar.setHidden(false);
  }

  render() {
    return (
      <View style={{ backgroundColor: '#0D4677', flex: 1, alignItems: 'center' }}>
        <Image
          style={{ height: '20%', width: '25%', resizeMode: 'contain', alignItems: 'center', marginTop: '55%' }}
          source={Assets.icon_splash}
        />
      </View>
    );
  }
}
