import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    Image,
    TouchableOpacity
} from 'react-native';

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import TabBar from '../Home/TabBar'
import Header from '../../components/HeaderMain'

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            list: []
        };

    }

    componentDidMount() {
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Header />
                <TabBar />
            </View>
        )
    }
}



export default Home
