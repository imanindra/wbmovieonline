import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    TouchableOpacity,
} from 'react-native';
import remoteConfig from '@react-native-firebase/remote-config';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Rating } from 'react-native-ratings';
import { Actions } from 'react-native-router-flux';
import TextCategory from '../../components/TextCategory';
import FastImage from 'react-native-fast-image';
import HeaderGenre from '../../components/HeaderGenre';
import DropDownPicker from 'react-native-dropdown-picker';

const bannerLink = remoteConfig().getValue('RN_banner');

class Genre extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            dataMovies: [],
            fetching_from_server: false,
            genre : 'action'
        };
        this.offset = 1;
    }

    componentDidMount() {
        this.getMovies()
    }


    getMovies() {
        fetch('https://api.gdriveplayer.us/v1/movie/search?genre='+this.state.genre)
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData != null) {
                    // this.offset = this.offset + 1;
                    this.setState({
                        // dataMovies: [...this.state.dataMovies, ...responseData]
                        dataMovies : responseData
                    })
                    console.log('123123',this.state.dataMovies)
                } else {
                    alert('error')
                }
            })
    }

    loadMoreData = async () => {
        this.setState({ fetching_from_server: true }, () => {
            fetch('https://api.gdriveplayer.us/v1/movie/search?genre='+this.state.genre+'&page=' + this.offset)
                .then(response => response.json())
                .then(responseJson => {
                    //Successful response from the API Call 
                    this.offset = this.offset + 1;
                    //After the response increasing the offset for the next API call.
                    this.setState({
                        dataMovies: [...this.state.dataMovies, ...responseJson],
                        //adding the new data with old one available in Data Source of the List
                        fetching_from_server: false,
                        //updating the loading state to false
                    });
                })
                .catch(error => {
                    console.error(error);
                });
        });
    };

    render() {
        const { dataMovies } = this.state
        return (
            <View style={{ flex: 1 }}>
                <HeaderGenre />

                <DropDownPicker
                    items={[
                        { label: 'Action', value: 'action' },
                        { label: 'Comedy', value: 'comedy' },
                        { label: 'Horror', value: 'horror' },
                        { label: 'Science-Fiction', value: 'science-fiction' },
                        { label: 'Adventure', value: 'adventure' },
                        { label: 'Crime', value: 'crime' },
                        { label: 'Mystery', value: 'mystery' },
                        { label: 'Thriller', value: 'thriller' },
                        { label: 'Adult', value: 'adult' },
                        { label: 'Drama', value: 'drama' },
                        { label: 'Romance', value: 'romance' },
                    ]}
                    defaultValue="action"
                    containerStyle={{ height: 40 }}
                    style={{ backgroundColor: '#fafafa' }}
                    dropDownStyle={{ backgroundColor: '#fafafa' }}
                    onChangeItem={item => 
                        this.setState({
                            genre : item.value
                        },() => this.getMovies())
                    }
                />
                <TextCategory
                    title={'NEWEST'}
                />

                <View style={{ alignItems: 'center', marginBottom: hp(10) }}>
                    <FlatList
                        data={dataMovies}
                        numColumns={2}
                        renderItem={({ item }) =>
                            <TouchableOpacity
                                style={{ alignItems: 'center' }}
                                onPress={() => Actions.Detail({ dataDetail: item })}>
                                <FastImage
                                    style={{ height: hp(35), width: wp(45), marginHorizontal: wp(2), resizeMode: 'contain', alignSelf: 'center', marginBottom: 2 }}
                                    source={{ uri: `${item.poster}`,
                                    priority: FastImage.priority.high, }}
                                />
                                <Text style={{ width: wp(48), textAlign: 'center' }}>{item.title}</Text>
                                <Rating
                                    readonly={true}
                                    ratingCount={5}
                                    imageSize={wp(5)}
                                    startingValue={item.rating / 2}
                                />
                                <Text>{item.rating}</Text>
                            </TouchableOpacity>
                        }
                        keyExtractor={item => item.id}
                        onEndReached={this.loadMoreData}
                        onEndReachedThreshold={0.1}
                    />
                </View>
            </View>
        )
    }
}

export default Genre
