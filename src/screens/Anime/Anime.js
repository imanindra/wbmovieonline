import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native';
import remoteConfig from '@react-native-firebase/remote-config';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { Actions } from 'react-native-router-flux';
import TextCategory from '../../components/TextCategory';
import FastImage from 'react-native-fast-image'
const bannerLink = remoteConfig().getValue('RN_banner');

class Anime extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            dataAnime: [],
            fetching_from_server: false,
        };
        this.offset = 1;
    }

    componentDidMount() {
        this.getAnime()
        console.log('anime', bannerLink.value)
    }

    getAnime() {
        fetch('https://api.gdriveplayer.us/v1/anime/newest?page=')
            .then((response) => response.json())
            .then((responseData) => {
                if (responseData != null) {
                    this.offset = this.offset + 1;
                    this.setState({
                        dataAnime: [...this.state.dataAnime, ...responseData]
                    })
                } else {
                    alert('error')
                }
            })
    }

    loadMoreData = async () => {
        this.setState({ fetching_from_server: true }, () => {
            fetch('https://api.gdriveplayer.us/v1/anime/newest?page=' + this.offset)
                .then(response => response.json())
                .then(responseJson => {
                    //Successful response from the API Call 
                    this.offset = this.offset + 1;
                    //After the response increasing the offset for the next API call.
                    this.setState({
                        dataAnime: [...this.state.dataAnime, ...responseJson],
                        //adding the new data with old one available in Data Source of the List
                        fetching_from_server: false,
                        //updating the loading state to false
                    });
                })
                .catch(error => {
                    console.error(error);
                });
        });
    };

    render() {
        const { dataAnime } = this.state
        return (
            <View style={{ flex: 1 }}>
                <View style={{ marginBottom: hp(10) }}>
                    <FastImage
                        style={{ width: wp(100), height: hp(8) }}
                        source={{ uri: bannerLink.value }}
                    />
                    <TextCategory
                        title={'NEWEST'}
                    />
                    <FlatList
                        data={dataAnime}
                        numColumns={2}
                        renderItem={({ item }) =>
                            <TouchableOpacity
                                style={{ alignItems: 'center', marginVertical: hp(0.5) }}
                                onPress={() => Actions.DetailAnime({ dataDetail: item, dataEpisodeCount: 1 })}>
                                 {item.poster != null &&
                                 <FastImage
                                    style={{ height: hp(35), width: wp(45), marginHorizontal: wp(2), resizeMode: 'contain', alignSelf: 'center', marginBottom: 2 }}
                                    source={{ uri: `${item.poster}` }}
                                />}
                                <Text style={{ width: wp(48), textAlign: 'center' }}>{item.title}</Text>
                                <View style={{ paddingHorizontal: wp(5), backgroundColor: 'red', borderRadius: wp(10) }}>
                                    <Text>Total Episode : {item.total_episode}</Text>
                                </View>
                            </TouchableOpacity>
                        }
                        keyExtractor={item => item.id}
                        onEndReached={this.loadMoreData}
                        onEndReachedThreshold={0.1}
                    />
                </View>
            </View>
        )
    }
}

export default Anime